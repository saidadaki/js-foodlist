export let renderFoodList = (foodArr) =>{
    let contentHTML = '';
    foodArr.forEach((item) => {
        let contentTr = `
            <tr>
                <td>${item.id}</td>
                <td>${item.name}</td>
                <td>${item.type ? "Chay":"Mặn"}</td>
                <td>${item.price}</td>
                <td>${item.discount}</td>
                <td>0</td>
                <td>${item.status?"Còn":"Hết"}</td>
                <td>
                    <button class="btn btn-danger" onclick="deleteFood('${item.id}')">Delete</button>
                    <button class="btn btn-info" onclick="editFood('${item.id}')">Edit</button>
                </td>
            </tr>
        `
        contentHTML += contentTr;
        
    });
    document.getElementById('tbodyFood').innerHTML = contentHTML;
}

/** toan tu ba ngoi?? su dung "?" va ":" */

export let onSuccess =(message) =>{
    Toastify({
        text: message,
        duration: 3000,
        destination: "https://github.com/apvarun/toastify-js",
        newWindow: true,
        close: true,
        gravity: "top", // `top` or `bottom`
        position: "right", // `left`, `center` or `right`
        stopOnFocus: true, // Prevents dismissing of toast on hover
        style: {
          background: "linear-gradient(to right, #00b09b, #96c93d)",
        },
        onClick: function(){} // Callback after click
      }).showToast();
}

// In thong tin len form
export let showThongTinLenForm=(item)=>{
    console.log("sun",item);
    document.getElementById("foodID").value = item.id;
    document.getElementById("tenMon").value = item.name;
    document.getElementById("loai").value = (item.type ?"Chay":"Mặn");
    document.getElementById("giaMon").value = item.price;
    document.getElementById("khuyenMai").value = item.discount;
    document.getElementById("tinhTrang").value = item.status;
    document.getElementById("hinhMon").value = item.img;
    document.getElementById("moTa").value = item.desc;
}


/**
 * sao no1 con2 loi64 ngay dong2 exort vay65 tar
 * tui cung hok bit nen moi hoi
 * thu tach ra xem sao
 */