import { layThongTinTuForm } from "../v1/controller-v1.js";
import { onSuccess, renderFoodList, showThongTinLenForm } from "./controller-v2.js";
// import { showThongTin } from "./controller-v2.js";
// true = chay, con 
// false = man, het 


// Get data from server
let BASE_URL = 'https://63f442dcfe3b595e2ef03b89.mockapi.io'
let fetchFoodList=() =>{
    axios({
        url : `${BASE_URL}/food`,
        method: "GET"
    }
    ).then(
        (res)=>{
            // console.log("res", res.data);
            renderFoodList(res.data);
        }
    ).catch(
        (err)=>{
            console.log("err", err)
        }
    )
}



fetchFoodList();

let deleteFood = (id) =>{
    axios({
        url: `${BASE_URL}/food/${id}`,
        method: "DELETE",
    })
    .then((res)=>{
        onSuccess("Successfully deleted")
        
        fetchFoodList();
    }).catch((err)=>{
        console.log(err);
    })
}
// Khi file main js co type la: module thi tim hieu them phan nay ??
window.deleteFood = deleteFood;

let createFood=() =>{
    let data = layThongTinTuForm();
    axios({
        url: `${BASE_URL}/food`,
        method: "POST",
        data: data,
    }).then((res) => {
            $("#exampleModal").modal("hide");
            onSuccess("Succesfully Added");
            fetchFoodList();
            // console.log(res);
        }).catch((err) => {
            console.log(err);
        });
};
window.createFood = createFood;

window.editFood=(id) =>{
    
    $("#exampleModal").modal("show");
    // call axios truoc roi moi call showThongTinLenForm
    
    axios({
        url: `${BASE_URL}/food/${id}`,
    }).then((res) => {
            // console.log(res);
            showThongTinLenForm(res.data)
           
        }).catch((err) => {
            console.log(err);
        });

}
window.saveEditFood=()=>{
    let data = layThongTinTuForm();
    axios({
        url: `${BASE_URL}/food`,
        method: "POST",
        data: data,
    }).then((res) => {

            onSuccess("Succesfully Changed");
            $("#exampleModal").modal("hide");
            fetchFoodList();
          })
          .catch((err) => {
           console.log(err);
          });
}
